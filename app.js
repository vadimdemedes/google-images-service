'use strict';

/**
 * Dependencies
 */

var express = require('express');
var client = require('google-images');
var cors = require('cors');


/**
 * App
 */

var app = express();

app.use(cors());

app.get('/api/v1/query', function (req, res) {
	var query = req.query.q;

	client.search(query, function (err, images) {
		images = images.map(function (image) {
			return image.url;
		});

		var response = [{
			type: 'gallery',
			value: images
		}];

		res.json(response);
	});
});

app.listen(process.env.PORT || 5000);
